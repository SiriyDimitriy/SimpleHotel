$.ready(function() {

    /*marking the coosen element of navbar*/
    $('nav>ul>li').each(function(index, el) {
        $(el).on('click', markNavBarElement);
    });

    function markNavBarElement() {
        $('nav>ul>li').each(function(index, el) {
            $(el).css('background-color', '');
        });
        $(this).css('background-color', '#969696');
        if (this == $('nav>ul>li')[0]) {
            $(this).css('border-radius', '10px 0 0 10px');
        }
    }

});
$(window).scroll(function() { //ie8 haven't scroll in document
    var scroll = $(window).scrollTop(), //for cross-browser support
        headerHeight = $('header').height();

    if (scroll > headerHeight) {
        $('nav').css({
            'position': 'fixed',
            'z-index': '2000',
            'border-left': 'none',
            'border-light': 'none',
            'border-top': 'none',
            'left': '0',
            'top': '0',
            'border-radius': '0px',
            'width': '100%'
        });
    } else {
        $('nav').attr('style', '')
    };

});

$(document).ready(function() {
    function loadMain() {
        $("main").load("htmlTemplates/mainContent.html");
    }
    loadMain();
    $('nav>ul>li:nth-child(1)').on('click', loadMain);

    function loadBookingForm() {
        $("main").empty();
        $("main").load("htmlTemplates/bookingForm.html");
    }
    $('nav>ul>li:nth-child(4)').on('click', loadBookingForm);

    function loadRoomsMap() {
        $.get('htmlTemplates/roomsMap.html', function(data) {
            $("main").empty();
            $("main").append(data);
            setImgForRoomState();
            checkStateAndAddImage();
            createTitle();
        });
    }
    $('nav>ul>li:nth-child(5)').on('click', loadRoomsMap);

    function checkStateAndAddImage() {
        $('[class^=room]').append('<img>').each(function(i, obj) {
            if ($(obj).attr('data-state') == 'free') {
                $(obj).children('img').attr('src', 'images/roomsMap/freeRoom.png');
            }
            if ($(obj).attr('data-state') == 'booked') {
                $(obj).children('img').attr('src', 'images/roomsMap/bookedRoom.png');
            }
        });
    }

    function createTitle() {
        $('[class^=room]').hover(function() {
            $('<div></div>').appendTo($(this)).addClass('roomTitle').text($(this).attr('data-title'));
        }, function() {
            $(this).children('div').remove();
        });
    }

    function setImgForRoomState(selectedDate) {
        var nextDay = createNextDateString();
        var selectedDate = selectedDate || nextDay;
        $.ajax({
                url: 'api/roomsmap?date=' + selectedDate,
                type: 'GET',
                dataType: 'json',
            })
            .done(function(data) {
                var obj = data;
                $.each(obj, function(index) {

                })
            })
            .fail(function() {
                console.log("error");
            });
    }

    function createNextDateString() {
        var next = new Date(new Date().getTime() + (24 * 60 * 60 * 1000));
        var year = next.getFullYear();
        var month = ("0" + (next.getMonth() + 1)).slice(-2);
        var day = ("0" + next.getDate()).slice(-2);
        var dateStr = year + "-" + month + "-" + day;
        return dateStr;
    }
});
