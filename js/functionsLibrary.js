function loadTemplate(templSrc) {
    return $.ajax({
            url: templSrc,
            type: 'GET',
            dataType: 'html',
        })
        .fail(function() {
            console.log('error of loading template ' + templSrc);
        });
}

function loadAndInsertTemplate(templSrc, templateDest) {
    loadTemplate(templSrc)
        .done(function(template) {
            $(templateDest).empty();
            $(templateDest).append(template);
        });
}

function createDateString(dayAmount) {
    var n = 0 || dayAmount;
    var day = new Date(new Date().getTime() + n * (24 * 60 * 60 * 1000));
    var year = day.getFullYear();
    var month = ("0" + (day.getMonth() + 1)).slice(-2);
    var day = ("0" + day.getDate()).slice(-2);
    var dateStr = year + "-" + month + "-" + day;
    return dateStr;
}

function sortAsNumbers(a, b) {
    return a - b;
}

function createArrayOfUniquePropertiesInObj(prop, obj) {
    var arr = [];
    $.each(obj, function(i) {
        arr.push(obj[i][prop]);
    });
    var unicArr = $.makeArray($(arr).filter(function(i, el) {
        return i == $.inArray(el, arr);
    }));
    return unicArr;
}

function createErrorMessageAfter(text, el) {
    $('<span></span>').insertAfter(el).addClass('errorMessageAfterInput').text(text);
}

function checkStringTemplateInDateInput(input) {
    if (input.val().search(/\d{4}-\d{2}-\d{2}/) == -1) {
        input.css({
            'border': '2px solid red'
        });
        createErrorMessageAfter(' invalid date, only "yyyy-mm-dd" format', input);
    }
}

function addErrorMessageAfterDataElement(earlierId, latestId, target) {

    function compareEarlierAndLatestDate() {
        var startMs = Date.parse($(earlierId).val());
        var endMs = Date.parse($(latestId).val());
        if (startMs > endMs) {
            $(earlierId).css({ 'border': '2px solid red' });
            createErrorMessageAfter(' invalid date, start date larger than end date', $(target));
        }
    }

    function removeStylesAndElements() {
        $(earlierId).attr('style', '').next('span').remove();
        $(latestId).attr('style', '').next('span').remove();
    }

    if ('#' + $(target).attr('id') == earlierId) {
        removeStylesAndElements();
        checkStringTemplateInDateInput($(target));
        checkStringTemplateInDateInput($(latestId));
        compareEarlierAndLatestDate($(earlierId), $(latestId));
    }
    if ('#' + $(target).attr('id') == latestId) {
        removeStylesAndElements();
        checkStringTemplateInDateInput($(target));
        checkStringTemplateInDateInput($(earlierId));
        compareEarlierAndLatestDate($(earlierId), $(latestId));
    }
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

function isValidName(name) {
    var pattern = new RegExp(/[A-Z][А-Я]ЁЇ{1}[a-z][а-я]ёї{1,15}/i);
    return pattern.test(name);
}

function isValidPhone(phone) {
    var pattern = new RegExp(/^((\+38)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{10}$/);
    return pattern.test(name);
}

Array.prototype.filter = function(filterFunc, paramsObj) {
    var res = [];
    for (var i = 0; i < this.length; i++) {
        if (filterFunc(this[i], paramsObj)) {
            res.push(this[i]);
        }
    }
    return res;
}