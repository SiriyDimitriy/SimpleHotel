    var datepickerSettings = {
        dateFormat: 'yy-mm-dd',
        constrainInput: true,
        minDate: "+1",
        changeYear: true,
        yearRange: "0:+1",
        beforeShow: function() {
            $(".ui-datepicker").css('font-size', '12px');
        }
    };

    function filterByType(arrElem, params) {
        return arrElem['type'] == params.type || params.type == 'notSelected';
    }

    function filterByPlaceCount(arrElem, params) {
        return arrElem['placesCount'] == params.placesCount || params.placesCount == 'notSelected';
    }

    function filterAndSortRooms(arr) {
        var selectedType = $('main').find('.byTypeSorting').val();
        var selectedNumb = $('main').find('.byPlaceCountSorting').val();
        return arr.filter(filterByType, { type: selectedType })
            .filter(filterByPlaceCount, { placesCount: selectedNumb })
            .sort(sortBy);
    }

    function sortBy(a, b) {
        var selectedElemVal = $('main').find('.sortBy').val();

        if (selectedElemVal == 'byPriceIncrease') {
            return a['price'] - b['price'];
        }
        if (selectedElemVal == 'byPriceDecrease') {
            return b['price'] - a['price'];
        }
        if (selectedElemVal == 'byNameAlph') {
            return a['id'] - b['id'];
        }
        if (selectedElemVal == 'byNameAlphReverse') {
            return b['id'] - a['id'];
        }
    }

    function buildRoomsList(data, template) {
        $.each(data, function(i) {
            $(template).appendTo("main");
            var templ = $('main>.item:last-child');
            templ.find('h3').text('№' + data[i]['id'] + ' ' + data[i]['type'] + ' ' + data[i]['placesCount'] + '-places room');
            templ.find('img').attr('src', data[i]['img'][0]);
            templ.find('.price').text(data[i]['price']);
            var arr = data[i]['description'].split(', ');
            $.each(arr, function(index) {
                $('<li></li>').appendTo(templ.find('ul')).text(arr[index]);
            });
        });
    }
    $(document).ready(function() {

        loadDropdowListForRoomsTypes();

        /*marking the coosen element of navbar*/
        $('nav>ul>li').each(function(index, el) {
            $(el).on('click', markNavBarElement);
        });

        loadAndInsertTemplate("htmlTemplates/mainContent.html", 'main'); //load main page template

        $('nav>ul>li:nth-child(1)').on('click', function() {
            loadAndInsertTemplate("htmlTemplates/mainContent.html", 'main');
        });

        function loadBookingForm() {
            return loadTemplate("htmlTemplates/bookingForm.html").done(function(template) {
                $('main').empty();
                $('main').append(template);
                $('#startDate').datepicker(datepickerSettings).val(createDateString(1));
                $('#endDate').datepicker(datepickerSettings).val(createDateString(1));
                $('#endDate,#startDate').on('keypress', function() { //prevent user mistake
                    return false;
                });

                $('#endDate,#startDate').on('change', function(event) {
                    addErrorMessageAfterDataElement('#startDate', '#endDate', event.target);
                }); //prevent user mistake by date selecting

                $("form").submit(function(event) {

                    event.preventDefault(); //prevent default behavior by form
                    var fields = ["name", "surname", "phone", "email"];
                    var error = 0;
                    $("form").find(".nameField,.surnameField,.phoneField,.emailField").each(function() { //prevent empty fields
                        var el = $(this);

                        $.each(fields, function(i) {
                            if ($(el).attr("name") == fields[i]) {
                                $(el).attr('style', '');
                                $(el).next('span').remove();
                                if (!$(el).val()) {
                                    $(el).css('border', '2px red solid');
                                    createErrorMessageAfter(' empty field', $(el));
                                    error = 1;
                                }
                            }
                        });
                    });

                    $("form").find(".nameField,.surnameField,.phoneField,.emailField").each(function() {
                        $(this).attr('style', '');
                        $(this).next('span').remove();
                    });

                    var name = $("main").find('.nameField').val();
                    if (!isValidName(name)) {
                        error = 2;
                        $(".nameField").css('border', '2px red solid');
                        createErrorMessageAfter(' incorrect name', $(".nameField"));
                    }

                    var surname = $("main").find('.surnameField').val();
                    if (!isValidName(surname)) {
                        error = 3;
                        $(".surnameField").css('border', '2px red solid');
                        createErrorMessageAfter(' incorrect surname', $(".surnameField"));
                    }

                    var phone = $("main").find('.phoneField').val();
                    if (!isValidPhone(phone)) {
                        error = 4;
                        $(".phoneField").css('border', '2px red solid');
                        createErrorMessageAfter(' incorrect phone number', $(".phoneField"));
                    }

                    var email = $("main").find('.emailField').val();
                    if (!isValidEmailAddress(email)) {
                        error = 5;
                        $(".emailField").css('border', '2px red solid');
                        createErrorMessageAfter(' incorrect e-mail', $(".emailField"));
                    }

                    if (error == 0) {
                        return true;
                    } else {
                        var err_text = "";
                        if (error == 1) err_text = "Не все обязательные поля заполнены!";
                        if (error == 2) err_text = "Введен не корректный e-mail!";
                        if (error == 3) err_text = "Пароли не совпадают";

                        $("#messenger").html(err_text);
                        $("#messenger").fadeIn("slow");
                        return false; //если в форме встретились ошибки , не  позволяем отослать данные на сервер.
                    }
                });
                $.get('api/roomsmap?date=' + createDateString(1)).done(function(data) {
                        var obj = data;

                        var typesList = createArrayOfUniquePropertiesInObj('type', obj);
                        $('<option></option>').appendTo('#roomType').text('room type');
                        $.each(typesList, function(i) {
                            $('<option></option>').appendTo('#roomType').text(typesList[i]);
                        });

                        var placesList = createArrayOfUniquePropertiesInObj('placesCount', obj).sort(sortAsNumbers);
                        $('<option></option>').appendTo('#roomPlaces').text('places count');
                        $.each(placesList, function(i) {
                            $('<option></option>').appendTo('#roomPlaces').text(placesList[i]);
                        });

                        var roomsList = createArrayOfUniquePropertiesInObj('id', obj).sort(sortAsNumbers);
                        $('<option></option>').appendTo('#roomsList').text('room number');
                        $.each(roomsList, function(i) {
                            $('<option></option>').appendTo('#roomsList').text(roomsList[i]);
                        });

                        var rooms;
                        $.get('api/roomsmap?date=' + createDateString(1)).done(function(data) {
                            rooms = data;
                            $('#roomType,#placesCount,#roomsList').on('change', separateDataAccordingToSelected);
                        });

                        function separateDataAccordingToSelected() {
                            var selected = $(this).find('option:selected').text();
                            if ($(this).attr('id') == 'roomType') {
                                rooms = $.makeArray($(rooms).filter(function(i) {
                                    return rooms[i]['type'] == selected;
                                }));

                                $('#roomType').empty();

                                $('<option></option>').appendTo('#roomType').text('room type');
                                $.each(typesList, function(i) {
                                    $('<option></option>').appendTo('#roomType').text(typesList[i]);
                                });
                            }
                            if ($(this).attr('id') == 'roomPlaces') {
                                rooms = $.makeArray($(rooms).filter(function(i) {
                                    return rooms[i]['placesCount'] == selected;
                                }));

                                $('#roomPlaces').empty();

                                $('<option></option>').appendTo('#roomPlaces').text('places count');
                                $.each(placesList, function(i) {
                                    $('<option></option>').appendTo('#roomPlaces').text(placesList[i]);
                                });
                            }
                            if ($(this).attr('id') == 'roomsList') {
                                rooms = $.makeArray($(rooms).filter(function(i) {
                                    return rooms[i]['id'] == selected;
                                }));

                                $('#roomsList').empty();

                                $('<option></option>').appendTo('#roomsList').text('room number');
                                $.each(rooms, function(i) {
                                    $('<option></option>').appendTo('#roomsList').text(rooms[i]['id']);
                                });
                            }
                        }

                    })
                    .fail(function() {
                        console.log("error of loading rooms list");
                    });
            });
        }

        $('nav>ul>li:nth-child(4)').on('click', loadBookingForm);

        function setRoomAttributes(data) {
            $.each(data, function(index) {
                var roomNumb = data[index]['id'];
                $('.room' + roomNumb).attr({
                    'data-state': data[index]['state'].toLowerCase(),
                    'data-title': data[index]['description'],
                    'data-room-number': data[index]['id']
                });
            });
            checkStateAndAddImage();
        }

        function isDateValid(date) {
            if (date.search(/\d{4}-\d{2}-\d{2}/) !== -1) {
                return true;
            }
            return false;
        }

        function reloadRoomsMap(event) {
            $(event.target).next('span').remove();
            if (isDateValid($(event.target).val())) {
                $.get('api/roomsmap?date=' + $(event.target).val(), setRoomAttributes);
            } else {
                createErrorMessageAfter(' invalid date, only "yyyy-mm-dd" format', $(event.target));
            }
        }

        function loadRoomsMap(event) {

            var nextDay = createDateString(1);
            var selectedDate = nextDay;
            var templatePromise = loadTemplate('htmlTemplates/roomsMap.html');
            var roomsPromise = $.get('api/roomsmap?date=' + selectedDate);
            $.when(templatePromise, roomsPromise).done(function(template, rooms) {
                $("main").empty();
                $("main").append(template[0]);

                var obj = rooms[0];
                setRoomAttributes(obj);
                checkStateAndAddImage();
                createTitle();

                $("#checkRoomsStates").datepicker(datepickerSettings).val(selectedDate);
                $('#checkRoomsStates').on('keypress', function() { //prevent user mistake
                    return false;
                });
                $('#checkRoomsStates').on('change', reloadRoomsMap); //reload map according to data selection
                //$('#checkRoomsStates').on('change', addErrorMessageAfterDataElement); //prevent user mistake
                $('[class^=room]').each(function(i, el) {
                    if ($(el).attr('data-state') == 'free') {
                        $(el).find('img').on('click', obj, directToBookingForm);
                    }
                });
            });
        }

        $('nav>ul>li:nth-child(5)').on('click', loadRoomsMap);

        $('nav>ul>li:nth-child(7)').on('click', function() {
            loadAndInsertTemplate("htmlTemplates/contacts.html", 'main');
        });
        $('header>.contactInfo>.address').on('click', function() {
            loadAndInsertTemplate("htmlTemplates/contacts.html", 'main');
        });
        /*    function setImgForRoomState(selectedDate) {
                var nextDay = createDateString(1);
                var selectedDate = selectedDate || nextDay;
                $('#checkRoomsStates').val(selectedDate);
                $.ajax({
                        url: 'api/roomsmap?date=' + selectedDate,
                        type: 'GET',
                        dataType: 'json',
                    })
                    .done(function(data) {
                        var obj = data;
                        $.each(obj, function(index) {
                            var roomNumb = obj[index]['id'];

                            $('.room' + roomNumb).attr({
                                'data-state': obj[index]['state'].toLowerCase(),
                                'data-title': obj[index]['description'],
                                'data-room-number': obj[index]['id']
                            });
                        });
                        checkStateAndAddImage();
                        createTitle();
                    })
                    .fail(function() {
                        console.log("error");
                    });
            }*/
        function directToBookingForm(event) {
            var roomNumber = $(this).parent('div').attr('data-room-number');
            var roomType;
            var roomPlaces;
            $.each(event.data, function(i) {
                if (event.data[i]['id'] == roomNumber) {
                    roomType = event.data[i]['type'];
                    roomPlaces = event.data[i]['placesCount'];
                }
            });

            loadBookingForm().done(setChoosenRoomToBookingForm);

            function setChoosenRoomToBookingForm() {
                $('#roomType>option').each(function() {
                    if ($(this).text() == roomType) {
                        $(this).attr('selected', 'selected');
                    }
                });
                $('#roomPlaces>option').each(function(i) {
                    if (parseInt($(this).text(), 10) == roomPlaces) {
                        $(this).attr('selected', 'selected');
                    }
                });
                $('#roomsList>option').each(function(i) {
                    if ($(this).text() == roomNumber) {
                        $(this).attr('selected', 'selected');
                    }
                });
            }
        }

        function createListOfSelectedFiltersIn(elem) {
            var selectedFilters = [];
            var el = $(elem).find('select');
            $.each(el, function(i) {
                if ($(el[i]).val() !== 'notSelected') {
                    selectedFilters.push($(el[i]).val());
                }
            });
            return selectedFilters;
        }

        function loadAllRooms() {
            $("main").empty();
            $.ajax({
                    url: 'api/rooms',
                    type: 'GET',
                    dataType: 'json',
                })
                .done(function(data) {

                    var templateRoomPromise = loadTemplate('htmlTemplates/singleRoom.html');
                    var templateFilterPromise = loadTemplate('htmlTemplates/filtersAndSortingElements.html');
                    $.when(templateRoomPromise, templateFilterPromise).done(function(roomTempl, filtersTempl) {
                        $(filtersTempl[0]).appendTo("main");

                        var modifiedData = filterAndSortRooms(data);
                        buildRoomsList(modifiedData, roomTempl[0]);

                        $('main').find('.sortBy').on('change', function(event) {

                            $("main").find('.item').remove();

                            var modifiedData = filterAndSortRooms(data);
                            buildRoomsList(modifiedData, roomTempl[0]);
                        });

                        $('main').find('.byNumberSorting').on('change', function(event) {

                            $("main").find('.item').remove();

                            var modifiedData = filterAndSortRooms(data);
                            buildRoomsList(modifiedData, roomTempl[0]);
                        });
                        $('main').find('.byPlaceCountSorting').on('change', function(event) {

                            $("main").find('.item').remove();

                            var modifiedData = filterAndSortRooms(data);
                            buildRoomsList(modifiedData, roomTempl[0]);
                        });

                        $('main').find('.byTypeSorting').on('change', function(event) {

                            $("main").find('.item').remove();

                            var modifiedData = filterAndSortRooms(data);
                            buildRoomsList(modifiedData, roomTempl[0]);
                        });
                    });
                })
                .fail(function() {
                    console.log("error");
                });
        }
        $('nav>ul>li:nth-child(2)').on('click', loadAllRooms);
    });
