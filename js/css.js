$(window).scroll(function() { //ie8 haven't scroll in document
    var scroll = $(window).scrollTop(), //for cross-browser support
        headerHeight = $('header').height();

    if (scroll > headerHeight) {
        $('nav').css({
            'position': 'fixed',
            'z-index': '2000',
            'border-left': 'none',
            'border-light': 'none',
            'border-top': 'none',
            'left': '0',
            'top': '0',
            'border-radius': '0px',
            'width': '100%'
        });
    } else {
        $('nav').attr('style', '');
    }
});

function markNavBarElement() {
    $('nav>ul>li').each(function(index, el) {
        $(el).css('background-color', '');
    });
    $(this).css('background-color', '#969696');
    if (this == $('nav>ul>li')[0]) {
        $(this).css('border-radius', '10px 0 0 10px');
    }
}

function loadDropdowListForRoomsTypes() {
    $.ajax({
            url: 'api/roomtypes',
            type: 'GET',
            dataType: 'json',
        })
        .done(function(data) {
            var obj = data;
            $.each(obj, function(i) {
                $('<li></li>').appendTo('nav>ul>li:nth-child(2)>ul').text(obj[i]);
            });
        })
        .fail(function() {
            console.log("error of room type loading");
        });
}

function checkStateAndAddImage() {
    $('[class^=room]').append('<img>').each(function(i, obj) {
        if ($(obj).attr('data-state') == 'free') {
            $(obj).children('img').attr('src', 'images/roomsMap/freeRoom.png');
        }
        if ($(obj).attr('data-state') == 'booked') {
            $(obj).children('img').attr('src', 'images/roomsMap/bookedRoom.png');
        }
    });
}

function createTitle() {
    $('[class^=room]').hover(function() {
        $('<div></div>').appendTo($(this)).addClass('roomTitle').text($(this).attr('data-title'));
    }, function() {
        $(this).children('div').remove();
    });
}
